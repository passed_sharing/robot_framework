*** Settings ***
Library    Process
Library    DataDriver    file=ddt_test_dada.xlsx    sheet_name=ddt_data        
Test Template    Is Triangle

*** Test Cases ***
Triangle program check: with sides ${a}, ${b}, ${c} does it result in ${expected_result}?

*** Keywords ***
Is Triangle
    [Arguments]    ${a}    ${b}    ${c}    ${expected_result}
    ${result}=    Run Process      python3    is_triangle.py    ${a}    ${b}    ${c}
    Should Be Equal    ${result.stdout}     ${expected_result}    Expected result: "${expected_result}", but the actual result "${result.stdout}".    False
